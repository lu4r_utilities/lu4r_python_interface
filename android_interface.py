#!/usr/bin/env python

__author__ = "Andrea Vanzo"

###  Imports  ###

# Python Libraries
import sys
import socket
import aiml
import datetime
import json

###  Variables  ###
conn = None
time = datetime.datetime.now()

###  Classes  ###
class AndroidInterface(object):
    ###  Interaction Variables  ###
    reply = ''
    
    def __init__(self):
        global conn

        HOST = ''
        PORT = 9001
        
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            print 'Socket created'
            try:
                s.bind((HOST, PORT))
            except socket.error as msg:
                print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
                sys.exit()
            print 'Socket bind complete'
            s.listen(10)

            print 'Socket now listening on port ' + str(PORT)
            
            currentFragment = ''
            while True:
                print "Waiting for connection on port " + str(PORT)
                conn, addr = s.accept()
                print 'Connected with ' + addr[0] + ':' + str(addr[1])
                while True:
                    data = conn.recv(2048)  # 512
                    print 'Received: ' + data
                    if data and not data.isspace():
                        if not data:
                            continue
                        if "REQ" in data:
                            data = data.replace("REQ", "")
                            conn.send("ACK\r\n")
                            continue
                        if '$' in data:
                            currentFragment = data[1:-1]
                            print 'You selected the ' + currentFragment + ' fragment'
                            continue
                        if currentFragment == "JOY":
                            print data
                        elif currentFragment == "SLU":
                            transcriptions = json.loads(data)
                            best_hypo = transcriptions['hypotheses'][0]['transcription']
                            print 'You said: ' + best_hypo
                    else:
                        print 'Disconnected from ' + addr[0] + ':' + str(addr[1])
                        break
            s.close()
        except socket.error as socketerror:
            print("Error: ", socketerror)


###  If Main  ###
if __name__ == '__main__':
    AndroidInterface()
