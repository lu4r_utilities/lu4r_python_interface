#!/usr/bin/env python
# encoding=utf8

import socket
import sys
import aiml
import sys
import getopt
import glob
import os
import requests
import json

lu4r_url = ''
HEADERS = {'content-type': 'application/json'}

def main(argv):

	aiml_kb_path = None
	using_aiml = False
	lu4r_ip = None
	lu4r_port = None
	HOST = ''
	PORT = 9000
	lu4r_addr_count = 0
	try:
		opts, args = getopt.getopt(argv,"hp:",["help","port=","aiml_kb=","lu4r_ip=","lu4r_port="])
	except getopt.GetoptError:
		print 'lu4r_interface.py\n\t--help[-h]\t\tShow this message.\n\t--port[-p]\t<port>\tSet the listening port.\n\t--aiml_kb\t<path>\tSet the directory containing the AIML Knowledge Bases.\n\t--lu4r_ip\t<ip>\tThe LU4R server IP address.\n\t--lu4r_port\t<port>\tThe LU4R server listening port.'
		sys.exit(2)
	
	for opt, arg in opts:
		if opt == '-h':
			print 'lu4r_interface.py\n\t--help[-h]\t\tShow this message.\n\t--port[-p]\t<port>\tSet the listening port.\n\t--aiml_kb\t<path>\tSet the directory containing the AIML Knowledge Bases.\n\t--lu4r_ip\t<ip>\tThe LU4R server IP address.\n\t--lu4r_port\t<port>\tThe LU4R server listening port.'
			sys.exit()
		elif opt in ("-p", "--port"):
			PORT = int(arg)
			print "\nListening port:\t" + str(PORT)
		elif opt in ("--aiml_kb"):
			aiml_kb_path = arg
			using_aiml = True
		elif opt in ("--lu4r_ip"):
			lu4r_ip = arg
			lu4r_addr_count = lu4r_addr_count + 1
		elif opt in ("--lu4r_port"):
			lu4r_addr_count = lu4r_addr_count + 1
			lu4r_port = arg
	if lu4r_addr_count == 2:
		lu4r_url = 'http://' + lu4r_ip + ':' + str(lu4r_port) + '/service/nlu'
		print "LU4R url:\t" + lu4r_url + "\n"
	else:
		print "Error! LU4R parameters not defined!\n"
		print 'lu4r_interface.py\n\t--help[-h]\t\tShow this message.\n\t--port[-p]\t<port>\tSet the listening port.\n\t--aiml_kb\t<path>\tSet the directory containing the AIML Knowledge Bases.\n\t--lu4r_ip\t<ip>\tThe LU4R server IP address.\n\t--lu4r_port\t<port>\tThe LU4R server listening port.'
		sys.exit()
	
	if using_aiml:
		k = aiml.Kernel()
		print "Importing AIML KBs"
		for file in glob.glob(aiml_kb_path + "/*.aiml"):
			k.learn(file)
		print "AIML KBs imported!"
	print "Number of categories: " + str(k.numCategories()) + "\n"
	
	
	########################################################
	#####################TO BE DELETED######################
	########################################################
	testing_frames = ["MOTION(goal:\"to the kitchen\",manner:\"slowly\")",
	"MOTION(manner:\"slowly\",goal:\"to the kitchen\")",
	"MOTION(goal:\"to the kitchen\")",
	"BRINGING(beneficiary:\"me\",theme:\"something\")",
	"BRINGING(beneficiary:\"me\",theme:\"something\")",
	"BRINGING(theme:\"something\",beneficiary:\"to someone\")",
	"BRINGING(theme:\"something\",goal:\"somewhere\")",
	"COTHEME(cotheme:\"me\",manner:\"slowly\")",
	"COTHEME(cotheme:\"me\")",
	"COTHEME(manner:\"slowly\",cotheme:\"me\")",
	"TAKING(theme:\"something\")"]
	for frame in testing_frames:
		frame = frame_to_aiml_format(frame)
		print k.respond(frame)
	########################################################
	#####################TO BE DELETED######################
	########################################################
	
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
	
	
	try:
		s.bind((HOST, PORT))
	except socket.error as msg:
		print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
		sys.exit()
	print 'Socket created!'
	s.listen(10)
	print 'Socket now listening on port ' + str(PORT)
	
	currentFragment = ''
	while 1:
		conn, addr = s.accept()
		print 'Connected with ' + addr[0] + ':' + str(addr[1])
		conn.send('Connected!' + '\r\n')
		while 1:
			data = conn.recv(512)
			print 'Received: ' + data
			if data and not data.isspace():
				if "REQ" in data:
					data = data.replace("REQ", "")
					conn.send("ACK\r\n")
				if '$' in data:
					current_fragment = data[1:-1]
					continue
				if current_fragment == "HOME":
					continue
				elif current_fragment == "JOY":
					print data
				elif current_fragment == "SLU":
					json_transcriptions = json.loads(data)
					best_transcription = json_transcriptions["hypotheses"][0]["transcription"]
					reply = ''
					#try to match best transcription with one of the pattern of the aiml kb
					if using_aiml:
						reply = k.respond(best_transcription)
						#if there is something that matches
						if reply != '':
							######################################
							#SEND THE REPLY TO ROBOT AND CONTINUE#
							#          TO BE IMPLEMENTED         #
							######################################
							print reply
							
						else:
							to_send = {'hypo': data, 'entities': '{\"entities\":[]}'}
							print data
							try:
								response = requests.post(lu4r_url, to_send, headers=HEADERS)
								lu4r_reply = frame_to_aiml_format(response.text)
								#retrieve the response and match it with an aiml pattern
								reply = k.respond(lu4r_reply)
							except requests.exceptions.RequestException as e:
								print 'ERROR! LU4R not running. Launch it and retry.'
							if reply == '':
								#there is no rule that matches the retrieved frame.
								#generate a sentence like "I don't understand"
								reply = "I'm sorry, but I don't understand."
							print reply
							######################################
							#SEND THE REPLY TO ROBOT AND CONTINUE#
							#          TO BE IMPLEMENTED         #
							######################################
					else:
						to_send = {'hypo': data, 'entities': '{\"entities\":[]}'}
						try:
							response = requests.post(lu4r_url, to_send, headers=HEADERS)
							print response.text
						except requests.exceptions.RequestException as e:
							print 'ERROR! LU4R not running. Launch it and retry.'
						print reply
				else:
					print "Unknown service"
			else:
				print 'Disconnected from ' + addr[0] + ':' + str(addr[1])
				current_fragment = ''
				break
	s.close()

def frame_to_aiml_format(frame):
	frame = frame.replace("(", " ")
	frame = frame.replace(")", "")
	frame = frame.replace("\"", " ")
	frame = frame.replace(":", " ")
	frame = frame.replace(",", " ")
	frame = frame.replace("  ", " ")
	return frame	

if __name__ == "__main__":
   main(sys.argv[1:])