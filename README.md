`lu4r_python_interface`
==============

This package enables for the communication between the [LU4R system][lu4r] and a computer through `python`programming language. The `lu4r_python_interface` is in charge of receiving the list of transcriptions from an external ASR and forward it to LU4R.

This ROS package provides a single `python` script:

* `android_interface`


## `android_interface`

It is an orchestrator between the robot, the Android application (for Automatic Speech Recognition) and LU4R. It communicates with the Android application, acting as a Server. Once the list of transcriptions is received from the Android app, it is forwarded to LU4R.

#### Requirements

* [Python 2.7][python]
* [pyAIML][aiml]

---

#### Installation

* Check-out the code of [`lu4r_python_interface`][repository]
* Install pyAIML
	* Check-out the code [here][aiml], or
	* run `pip install aiml`

---

#### Run

In order to start `lu4r_python_interface` python script, run:

~~~
python android_interface.py -p <server_listening_port> --aiml_kb <path_to_aiml_folder> --lu4r_ip <lu4r_ip_address> --lu4r_port <lu4r_listening_port>
~~~

where:

* `-p`: the listening port of the LU4R python interface. This is required by the Android app, enabling a TCP connection between them.
* `--aiml_kb `: the path to the folder containing all the AIML KBs that can be used along with LU4R. This feature enable a sort of controlled dialogue management.
* `--lu4r_ip`: the ip address of LU4R. If the LU4R and the LU4R ROS interface are on the same machine, ignore this argument.
* `--lu4r_port`: the listening port of LU4R.

`python android_interface.py -h` to prompt running instructions.

Running example:

~~~
python android_interface.py -p 9000 --aiml_kb alice_aiml_kb --lu4r_ip 127.0.0.1 --lu4r_port 9001
~~~

---

For more information, please visit the [LU4R system][lu4r] website.

[lu4r]: http://sag.art.uniroma2.it/sluchain.html
[python]: https://www.python.org/
[aiml]:https://github.com/creatorrr/pyAIML
[repository]:http